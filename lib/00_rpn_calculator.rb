class RPNCalculator
  def initialize(calculator = [])
    @calculator = calculator
  end

  def push(num)
    @calculator.push(num)
  end

  def plus
    raise Exception.new('calculator is empty') if @calculator.length < 2
    sum = @calculator[-1] + @calculator[-2]
    @calculator.pop
    @calculator.pop
    @calculator.push(sum)
  end

  def value
    @calculator[-1]
  end

  def minus
    raise Exception.new('calculator is empty') if @calculator.length < 2
    difference = @calculator[-2] - @calculator[-1]
    @calculator.pop
    @calculator.pop
    @calculator.push(difference)
  end

  def divide
    raise Exception.new('calculator is empty') if @calculator.length < 2
    quotient = @calculator[-2].to_f / @calculator[-1].to_f
    @calculator.pop
    @calculator.pop
    @calculator.push(quotient)
  end

  def times
    raise Exception.new('calculator is empty') if @calculator.length < 2
    product = @calculator[-2].to_f * @calculator[-1].to_f
    @calculator.pop
    @calculator.pop
    @calculator.push(product)
  end

  def tokens(string)
    new_arr = []
    string.each_char do |ch|
      if '1234567879'.include?(ch)
        new_arr << ch.to_i
      elsif '+-*/'.include?(ch)
        new_arr << ch.to_sym
      end
    end
    new_arr
  end

  def evaluate(string)
    to_calc = RPNCalculator.new
    tokens(string).each do |i|
      if i.is_a? Integer
        to_calc.push(i)
      elsif i == :+
        to_calc.plus
      elsif i == :-
        to_calc.minus
      elsif i == :*
        to_calc.times
      elsif i == :/
        to_calc.divide
      end
    end
    to_calc.value
  end
# :D
end
